var spawn = require("child_process").fork;
var timeout = 30; // minutes
var config = require("./config.json");
var path = require("path");

var env = process.env.HOSTNAME ? process.env.HOSTNAME: "default";

var EXEC_NODE = config.env[env].EXEC_NODE;
var EXEC_MONGODB = config.env[env].EXEC_MONGODB;
var EXEC_MONGODB_REST = config.env[env].EXEC_MONGODB_REST;
var EXEC_MONGODB_ADMIN = config.env[env].EXEC_MONGODB_ADMIN;

var PATH_NODE = config.env[env].PATH_NODE;
var PATH_MONGODB = config.env[env].PATH_MONGODB;
var PATH_MONGODB_REST = config.env[env].PATH_MONGODB_REST;
var PATH_MONGODB_ADMIN = config.env[env].PATH_MONGODB_ADMIN;

console.log("env", env);

console.log("EXEC_NODE", EXEC_NODE);
console.log("EXEC_MONGODB", EXEC_MONGODB);
console.log("EXEC_MONGODB_REST", EXEC_MONGODB_REST);
console.log("EXEC_MONGODB_ADMIN", EXEC_MONGODB_ADMIN);

console.log("PATH_NODE", PATH_NODE);
console.log("PATH_MONGODB", PATH_MONGODB);
console.log("PATH_MONGODB_REST", PATH_MONGODB_REST);
console.log("PATH_MONGODB_ADMIN", PATH_MONGODB_ADMIN);

// mongodb rest
console.log("start:", EXEC_MONGODB_REST, "in", PATH_MONGODB_REST);
spawn(PATH_MONGODB_REST+EXEC_MONGODB_REST, ["--port", config.db.port], {
    stdio: [process.stdin, process.stdout, process.stderr, 'ipc']
});

// mongodb express
// console.log("start:", EXEC_MONGODB_ADMIN, "in", PATH_MONGODB_ADMIN);
// spawn(PATH_MONGODB_ADMIN+EXEC_MONGODB_ADMIN,["--port", config.admin.port, "--host", config.db.host, "--dbport", config.db.port], {
//     stdio: [process.stdin, process.stdout, process.stderr, 'ipc']
// });