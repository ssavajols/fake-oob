declare module "components/dispatcher" {
    /**
     * @desc
     * 1. Register events on document
     * 2. Dispatch events on document
     */
    export default class dispatcher {
        static dispatch(name: string, data?: object): void;
        static register(name: string, callback: (event: Event) => void): void;
    }
}
declare module "components/state-manager" {
    export default class stateManager {
        private static states;
        private static history;
        private static currentState;
        static init(): void;
        static clear(): void;
        static goBack(n: number): void;
        static show(id: string, data?: any, ignoreHistory?: boolean): void;
        static getStates(): void;
    }
}
declare module "components/notification-entity" {
    export default class notificationEntity {
        /**
         * @desc title to display
         */
        title: string;
        /**
         * @desc id generated by mongodb
         */
        id: string;
        /**
         * @desc custom notification id
         */
        notifId: string;
        /**
         * @desc message showed in notification
         */
        message: string;
        /**
         * @desc enable sound when receive notification on device
         */
        soundEnabled: boolean;
        /**
         * @desc number to increment badge
         */
        badgeNumber: number;
        /**
         * @desc custom data
         */
        userData: any;
        /**
         * @desc message displayed in notification list
         */
        contentBody: string;
        /**
         * @desc title displayed in notification list
         */
        contentTitle: string;
        /**
         * @desc date of the notification
         */
        date: Date;
        /**
         * @desc creation notification date
         */
        created_at: Date;
        constructor(params: notificationEntity);
        /**
         * @desc convert notification into HTML string
         */
        toHTML(format?: string): string;
        /**
         * @desc convert notification into HTML DOM element
         */
        toDOMNode(element?: string, format?: string): HTMLElement;
        getTitle(): string;
        protected _getListHTML(): string;
        protected _getDetailHTML(): string;
        /**
         * @desc format date to notification format
         */
        protected _formatDate(date: Date): string;
    }
}
declare module "devices/deviceModel" {
    export default class deviceModel {
        name: string;
        devices: any;
        constructor(name: string, devices: Object);
    }
}
declare module "devices/devices" {
    import deviceModel from "devices/deviceModel";
    export default class devices {
        static list: deviceModel[];
        static getTarget(): deviceModel;
        static setTarget(target: string): string;
        static getConfigurationByName(name: string): deviceModel | null;
    }
}
declare module "configuration/env/env.dawtlx05" {
    export default class dawtlx05 {
        static db_host: string;
        static db_type: DB_TYPE;
        static static_server: string;
        static signature_URL: string;
        static alerting_URL: string;
    }
}
declare module "configuration/env/env.local" {
    export default class local {
        static db_host: string;
        static db_type: DB_TYPE;
        static static_server: string;
        static signature_URL: string;
        static alerting_URL: string;
    }
}
declare module "configuration/env/env.custom" {
    export default class local {
        static db_host: string;
        static db_type: DB_TYPE;
        static static_server: string;
        static signature_URL: string;
        static alerting_URL: string;
    }
}
declare module "configuration/env/index" {
    import envDawtlx05 from "configuration/env/env.dawtlx05";
    import envLocal from "configuration/env/env.local";
    import envCustom from "configuration/env/env.custom";
    export const dawtlx05: typeof envDawtlx05;
    export const local: typeof envLocal;
    export const custom: typeof envCustom;
}
declare module "configuration/env/env" {
    export const env: any;
}
declare module "configuration/mongodbModel" {
    export default class mongodbModel {
        static getUri(collection: string, id: string): string;
    }
}
declare module "configuration/oobModel" {
    export default class oobModel {
        static polling: number;
        static uri: string;
    }
}
declare module "configuration/removeActionsModel" {
    export default class remoteActionsModel {
        static polling: number;
        static uri: string;
    }
}
declare module "components/natif" {
    export class Natif {
        static moveToBackground(): void;
        static moveToForground(): void;
        static enableBackgroundMode(active: boolean): void;
        static unlockScreenOrientation(): void;
        static lockPortraitScreenOrientation(): void;
        static lockLandscapeScreenOrientation(): void;
    }
}
declare module "components/frame-messaging" {
    export const frameMessaging: {
        init: () => void;
    };
}
declare module "configuration/configuration" {
    import devices from "devices/devices";
    import mongodbModel from "configuration/mongodbModel";
    import oobModel from "configuration/oobModel";
    import remoteActionsModel from "configuration/removeActionsModel";
    export { env } from "configuration/env/env";
    export const localStorage: boolean;
    export const mongodb: typeof mongodbModel;
    export const oob: typeof oobModel;
    export const remoteActions: typeof remoteActionsModel;
    export const device: typeof devices;
}
declare module "components/local-notifications" {
    import notificationEntity from "components/notification-entity";
    /**
     *
     * @desc
     * 1. Create local notification component
     * 2. Check the type of the notification to send
     * 3. Send the notification
     */
    export default class localNotifications {
        private static localService;
        static init(): void;
        static registerLocalService(): void;
        static getLocalService(): any;
        static send(notification: notificationEntity): any;
    }
}
declare module "components/notification-entity-change-adresse" {
    import notificationEntity from "components/notification-entity";
    export default class notificationEntityChangeAdresse extends notificationEntity {
        title: string;
        protected _getListHTML(): string;
        protected _getDetailHTML(): string;
    }
}
declare module "components/notification-entity-credit-conso" {
    import notificationEntity from "components/notification-entity";
    export default class notificationEntityCreditConso extends notificationEntity {
        title: string;
        protected _getListHTML(): string;
        protected _getDetailHTML(): string;
    }
}
declare module "components/notification-entity-lep" {
    import notificationEntity from "components/notification-entity";
    export default class notificationEntityLEP extends notificationEntity {
        title: string;
        protected _getListHTML(): string;
        protected _getDetailHTML(): string;
    }
}
declare module "components/notification-factory" {
    import notificationEntity from "components/notification-entity";
    /**
     * @desc generate notification types objects
     */
    export default class notificationFactory {
        static create(notification: any): notificationEntity;
    }
}
declare module "components/list-notification" {
    import notificationEntity from "components/notification-entity";
    export default class listNotification {
        private static list;
        private static container;
        private static localStorageEnabled;
        static init(): void;
        static cleanStorage(): void;
        static save(): void;
        static restore(): void;
        static sync(notifications: notificationEntity[]): void;
        static add(notification: notificationEntity): void;
        static clear(): void;
        static remove(notification: notificationEntity): void;
        static update(): void;
        static exist(notification: notificationEntity): boolean;
    }
}
declare module "components/poller" {
    /**
     * @desc
     * 1. Create polling
     */
    export default class Poller {
        private timerInstance;
        private isActive;
        private options;
        constructor(params?: PollerOptions);
        setActive(bool: boolean): void;
        getActive(): boolean;
        loop(): void;
        destroy(): void;
    }
}
declare module "components/oob-lookup" {
    import notificationEntity from "components/notification-entity";
    /**
     * @desc
     * 1. Create polling
     * 2. Call db to get notification list
     * 3. Remove validated notification from db collection
     */
    export default class oobLookup {
        private static polling;
        private static uri;
        static start(callback: callback): void;
        static createPoller(params: any): void;
        static remove(notification: notificationEntity): void;
        static doRequest(callback: callback): void;
    }
}
declare module "components/remote-actions" {
    /**
     * @desc
     * 1. Create poller to remote-actions db collection
     * 2. Run action when received one
     * 3. Remove action from the remote-actions db collection
     */
    export default class remoteActions {
        private static polling;
        private static uri;
        static init(): void;
        static createPoller(): void;
        static doAction(type: string, data: any): void;
        static sendAction(data: any): Promise<string>;
    }
}
declare module "index" {
}
declare module "modules/signature/index" {
    import "index";
}
