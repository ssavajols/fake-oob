var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
define("components/dispatcher", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * @desc
     * 1. Register events on document
     * 2. Dispatch events on document
     */
    var dispatcher = /** @class */ (function () {
        function dispatcher() {
        }
        dispatcher.dispatch = function (name, data) {
            var evt = new Event(name);
            evt.data = data;
            document.dispatchEvent(evt);
        };
        dispatcher.register = function (name, callback) {
            document.addEventListener(name, callback);
        };
        return dispatcher;
    }());
    exports.default = dispatcher;
});
define("components/state-manager", ["require", "exports", "components/dispatcher"], function (require, exports, dispatcher_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var stateManager = /** @class */ (function () {
        function stateManager() {
        }
        stateManager.init = function () {
            stateManager.getStates();
            if (this.states[0]) {
                stateManager.show(this.states[0].dataset.state);
            }
        };
        stateManager.clear = function () {
            this.history.length = 0;
        };
        stateManager.goBack = function (n) {
            var state = null;
            if (!n) {
                n = 1;
            }
            while (n > 0) {
                state = this.history.pop();
                // skip current state
                if (state && state.id === this.currentState.id) {
                    continue;
                }
                n--;
            }
            if (state) {
                stateManager.show(state.id, state.data, true);
            }
        };
        stateManager.show = function (id, data, ignoreHistory) {
            if (!ignoreHistory) {
                this.history.push(this.currentState);
            }
            for (var _i = 0, _a = this.states; _i < _a.length; _i++) {
                var state = _a[_i];
                if (state.dataset.state === id) {
                    dispatcher_1.default.dispatch("page-" + state.dataset.state + "-show", data);
                    this.currentState = { id: id, data: data };
                    state.classList.add("show");
                    state.classList.remove("hide");
                }
                else {
                    dispatcher_1.default.dispatch("page-" + state.dataset.state + "-hide", data);
                    state.classList.add("hide");
                    state.classList.remove("show");
                }
            }
        };
        stateManager.getStates = function () {
            this.states = document.querySelectorAll("[data-state]");
        };
        stateManager.history = [];
        return stateManager;
    }());
    exports.default = stateManager;
});
define("components/notification-entity", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var notificationEntity = /** @class */ (function () {
        function notificationEntity(params) {
            /**
             * @desc title to display
             */
            this.title = " ";
            this.id = params.id || "";
            this.notifId = params.notifId || "";
            this.message = params.message || "";
            this.soundEnabled = params.soundEnabled || true;
            this.badgeNumber = params.badgeNumber || 1;
            this.userData = params.userData || {};
            this.contentBody = params.contentBody || "";
            this.contentTitle = params.contentTitle || "";
            this.date = params.date instanceof Date ? params.date : new Date(params.date) || new Date();
            this.created_at = params.date instanceof Date ? params.date : new Date(params.date) || new Date();
        }
        /**
         * @desc convert notification into HTML string
         */
        notificationEntity.prototype.toHTML = function (format) {
            if (format === void 0) { format = "list"; }
            switch (format) {
                case "detail":
                    return this._getDetailHTML();
                case "list":
                default:
                    return this._getListHTML();
            }
        };
        /**
         * @desc convert notification into HTML DOM element
         */
        notificationEntity.prototype.toDOMNode = function (element, format) {
            if (element === void 0) { element = "li"; }
            if (format === void 0) { format = "list"; }
            var NODE = document.createElement(element);
            NODE.innerHTML = this.toHTML(format);
            NODE.classList.add("notification-entity");
            return NODE;
        };
        notificationEntity.prototype.getTitle = function () {
            return this.title;
        };
        notificationEntity.prototype._getListHTML = function () {
            var html;
            html = '<img src="./img/alert-1-empty.png" width="100%" alt="">';
            html += "<span class=\"message\">" + this.contentBody + "</span>";
            return html;
        };
        notificationEntity.prototype._getDetailHTML = function () {
            var html;
            html = "";
            if (this.userData.title) {
                html += "<strong>" + this.userData.title + "</strong><br /><br />";
            }
            for (var key in this.userData) {
                if (key === "title" || key === "type") {
                    continue;
                }
                html += "<div><span>" + key + " : </span><span class=\"light\">" + this.userData[key] + "</span></div>";
            }
            return html;
        };
        /**
         * @desc format date to notification format
         */
        notificationEntity.prototype._formatDate = function (date) {
            var day = date.getDate();
            var monthIndex = date.getMonth() + 1;
            var year = date.getFullYear();
            return day + "/" + monthIndex + "/" + year + " - " + date.getHours() + ":" + date.getMinutes();
        };
        return notificationEntity;
    }());
    exports.default = notificationEntity;
});
define("devices/deviceModel", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var deviceModel = /** @class */ (function () {
        function deviceModel(name, devices) {
            this.name = name;
            this.devices = devices;
        }
        return deviceModel;
    }());
    exports.default = deviceModel;
});
define("devices/devices", ["require", "exports", "devices/deviceModel"], function (require, exports, deviceModel_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var devices = /** @class */ (function () {
        function devices() {
        }
        devices.getTarget = function () {
            var device;
            if (window.localStorage.getItem("device-id") === null) {
                devices.setTarget(JSON.stringify(devices.list[0]));
            }
            try {
                var parsed = JSON.parse(window.localStorage.getItem("device-id"));
                device = new deviceModel_1.default(parsed.name, parsed.devices);
            }
            catch (e) {
                console.info("no valid device configuration in localStorage");
            }
            return device;
        };
        devices.setTarget = function (target) {
            window.localStorage.setItem("device-id", JSON.stringify(target));
            return target;
        };
        devices.getConfigurationByName = function (name) {
            var config = null;
            devices.list.forEach(function (item) {
                if (item.name === name) {
                    config = item;
                }
            });
            return config;
        };
        devices.list = [
            new deviceModel_1.default("show-room-group-1", {
                agence: "show-room-group-1-desktop-3",
                back: "show-room-group-1-desktop-2",
                front: "show-room-group-1-desktop-1",
                mobile: "show-room-group-1-mobile-1",
            }),
            new deviceModel_1.default("show-room-group-2", {
                agence: "show-room-group-2-desktop-3",
                back: "show-room-group-2-desktop-2",
                front: "show-room-group-2-desktop-1",
                mobile: "show-room-group-2-mobile-1",
            }),
            new deviceModel_1.default("show-room-group-3", {
                agence: "show-room-group-3-desktop-3",
                back: "show-room-group-3-desktop-2",
                front: "show-room-group-3-desktop-1",
                mobile: "show-room-group-3-mobile-1",
            }),
            new deviceModel_1.default("show-room-group-4", {
                agence: "show-room-group-4-desktop-3",
                back: "show-room-group-4-desktop-2",
                front: "show-room-group-4-desktop-1",
                mobile: "show-room-group-4-mobile-1",
            }),
            new deviceModel_1.default("show-room-group-5", {
                agence: "show-room-group-5-desktop-3",
                back: "show-room-group-5-desktop-2",
                front: "show-room-group-5-desktop-1",
                mobile: "show-room-group-5-mobile-1",
            }),
            new deviceModel_1.default("show-room-group-6", {
                agence: "show-room-group-6-desktop-3",
                back: "show-room-group-6-desktop-2",
                front: "show-room-group-6-desktop-1",
                mobile: "show-room-group-6-mobile-1",
            }),
            new deviceModel_1.default("show-room-group-7", {
                agence: "show-room-group-7-desktop-3",
                back: "show-room-group-7-desktop-2",
                front: "show-room-group-7-desktop-1",
                mobile: "show-room-group-7-mobile-1",
            }),
            new deviceModel_1.default("show-room-group-8", {
                agence: "show-room-group-8-desktop-3",
                back: "show-room-group-8-desktop-2",
                front: "show-room-group-8-desktop-1",
                mobile: "show-room-group-8-mobile-1",
            }),
            new deviceModel_1.default("show-room-group-9", {
                agence: "show-room-group-9-desktop-3",
                back: "show-room-group-9-desktop-2",
                front: "show-room-group-9-desktop-1",
                mobile: "show-room-group-9-mobile-1",
            }),
            new deviceModel_1.default("stand-group-1", {
                agence: "stand-group-1-desktop-3",
                back: "stand-group-1-desktop-2",
                front: "stand-group-1-desktop-1",
                mobile: "stand-group-1-mobile-1",
            }),
            new deviceModel_1.default("stand-group-2", {
                agence: "stand-group-2-desktop-3",
                back: "stand-group-2-desktop-2",
                front: "stand-group-2-desktop-1",
                mobile: "stand-group-2-mobile-1",
            }),
            new deviceModel_1.default("stand-group-3", {
                agence: "stand-group-3-desktop-3",
                back: "stand-group-3-desktop-2",
                front: "stand-group-3-desktop-1",
                mobile: "stand-group-3-mobile-1",
            }),
            new deviceModel_1.default("stand-group-4", {
                agence: "stand-group-4-desktop-3",
                back: "stand-group-4-desktop-2",
                front: "stand-group-4-desktop-1",
                mobile: "stand-group-4-mobile-1",
            }),
            new deviceModel_1.default("stand-group-5", {
                agence: "stand-group-5-desktop-3",
                back: "stand-group-5-desktop-2",
                front: "stand-group-5-desktop-1",
                mobile: "stand-group-5-mobile-1",
            }),
            new deviceModel_1.default("stand-group-6", {
                agence: "stand-group-6-desktop-3",
                back: "stand-group-6-desktop-2",
                front: "stand-group-6-desktop-1",
                mobile: "stand-group-6-mobile-1",
            }),
            new deviceModel_1.default("stand-group-7", {
                agence: "stand-group-7-desktop-3",
                back: "stand-group-7-desktop-2",
                front: "stand-group-7-desktop-1",
                mobile: "stand-group-7-mobile-1",
            }),
            new deviceModel_1.default("stand-group-8", {
                agence: "stand-group-8-desktop-3",
                back: "stand-group-8-desktop-2",
                front: "stand-group-8-desktop-1",
                mobile: "stand-group-8-mobile-1",
            }),
            new deviceModel_1.default("stand-group-9", {
                agence: "stand-group-9-desktop-3",
                back: "stand-group-9-desktop-2",
                front: "stand-group-9-desktop-1",
                mobile: "stand-group-9-mobile-1",
            }),
        ];
        return devices;
    }());
    exports.default = devices;
});
// import "../../declarations/lib";
define("configuration/env/env.dawtlx05", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SERVICE_IP_VALUE = localStorage.getItem('SERVICE_IP_VALUE');
    var dawtlx05 = /** @class */ (function () {
        function dawtlx05() {
        }
        dawtlx05.db_host = 'http://awt.bddf.applis.bad.socgen/show-room-mongo-rest/db/:collection/:id';
        dawtlx05.db_type = 'mongodb-rest';
        dawtlx05.static_server = "http://localhost:8080";
        dawtlx05.signature_URL = "http://awt.bddf.applis.bad.socgen/show-room-statics/fake_signature_mobile/signature/mesDemandesAccueil.html";
        dawtlx05.alerting_URL = "http://awt.bddf.applis.bad.socgen/show-room-statics/alerting/";
        return dawtlx05;
    }());
    exports.default = dawtlx05;
});
// import "../../declarations/lib";
define("configuration/env/env.local", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SERVICE_IP_VALUE = localStorage.getItem('SERVICE_IP_VALUE');
    var local = /** @class */ (function () {
        function local() {
        }
        local.db_host = "https://api.mlab.com/api/1/databases/sg-show-room/collections/:collection/:id?apiKey=m_NgRFUKXiviyuWoHLX2Xk8tP9yQ4zaF";
        local.db_type = "mongolab";
        local.static_server = "http://localhost:8080";
        local.signature_URL = "http://localhost:8080";
        local.alerting_URL = "http://localhost:8080";
        return local;
    }());
    exports.default = local;
});
// import '../../declarations/lib';
define("configuration/env/env.custom", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SERVICE_IP_VALUE = localStorage.getItem('SERVICE_IP_VALUE');
    var local = /** @class */ (function () {
        function local() {
        }
        local.db_host = "http://" + SERVICE_IP_VALUE + ":27023/db/:collection/:id";
        local.db_type = 'mongodb-rest';
        local.static_server = "http://" + SERVICE_IP_VALUE + ":8080";
        local.signature_URL = "http://" + SERVICE_IP_VALUE + ":8080";
        local.alerting_URL = "http://" + SERVICE_IP_VALUE + ":8080";
        return local;
    }());
    exports.default = local;
});
define("configuration/env/index", ["require", "exports", "configuration/env/env.dawtlx05", "configuration/env/env.local", "configuration/env/env.custom"], function (require, exports, env_dawtlx05_1, env_local_1, env_custom_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.dawtlx05 = env_dawtlx05_1.default;
    exports.local = env_local_1.default;
    exports.custom = env_custom_1.default;
});
define("configuration/env/env", ["require", "exports", "configuration/env/index"], function (require, exports, envConfig) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var _env;
    switch (window.localStorage.getItem('SERVICE_IP')) {
        case 'local':
            _env = envConfig.local;
            break;
        case 'custom':
            _env = envConfig.custom;
            break;
        default:
            _env = envConfig.dawtlx05;
    }
    exports.env = _env;
});
define("configuration/mongodbModel", ["require", "exports", "configuration/env/env"], function (require, exports, env_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var mongodbModel = /** @class */ (function () {
        function mongodbModel() {
        }
        mongodbModel.getUri = function (collection, id) {
            var hostname = env_1.env.db_host;
            var type = env_1.env.db_type;
            if (type === 'mongolab' || type === 'mongodb-rest') {
                return hostname.replace(':collection', collection).replace(':id', id);
            }
            return '';
        };
        return mongodbModel;
    }());
    exports.default = mongodbModel;
});
define("configuration/oobModel", ["require", "exports", "configuration/mongodbModel"], function (require, exports, mongodbModel_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var oobModel = /** @class */ (function () {
        function oobModel() {
        }
        oobModel.polling = 3000;
        oobModel.uri = mongodbModel_1.default.getUri("oob", ":id");
        return oobModel;
    }());
    exports.default = oobModel;
});
define("configuration/removeActionsModel", ["require", "exports", "configuration/mongodbModel"], function (require, exports, mongodbModel_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var remoteActionsModel = /** @class */ (function () {
        function remoteActionsModel() {
        }
        remoteActionsModel.polling = 3000;
        remoteActionsModel.uri = mongodbModel_2.default.getUri("remote_actions", ":id");
        return remoteActionsModel;
    }());
    exports.default = remoteActionsModel;
});
define("components/natif", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Natif = /** @class */ (function () {
        function Natif() {
        }
        Natif.moveToBackground = function () {
            if (typeof cordova !== 'undefined') {
                cordova.plugins.backgroundMode.moveToBackground();
            }
        };
        Natif.moveToForground = function () {
            if (typeof cordova !== 'undefined') {
                cordova.plugins.backgroundMode.moveToForground();
            }
        };
        Natif.enableBackgroundMode = function (active) {
            if (typeof cordova !== 'undefined') {
                cordova.plugins.backgroundMode.setEnabled(active);
            }
        };
        Natif.unlockScreenOrientation = function () {
            if (typeof cordova !== 'undefined') {
                screen.orientation.unlock();
            }
        };
        Natif.lockPortraitScreenOrientation = function () {
            if (typeof cordova !== 'undefined') {
                screen.orientation.lock('portrait');
            }
        };
        Natif.lockLandscapeScreenOrientation = function () {
            if (typeof cordova !== 'undefined') {
                screen.orientation.lock('landscape');
            }
        };
        return Natif;
    }());
    exports.Natif = Natif;
});
define("components/frame-messaging", ["require", "exports", "components/natif"], function (require, exports, natif_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.frameMessaging = {
        init: function () {
            window.addEventListener('message', function (event) {
                var data = JSON.parse(event.data);
                console.log('received message:', event);
                switch (data.type) {
                    case "syncLocalStorageRequest":
                        Array.prototype.forEach.call(document.querySelectorAll('iframe'), function (iframe) {
                            iframe.contentWindow.postMessage(JSON.stringify({
                                'type': 'syncLocalStorage',
                                'SERVICE_IP': localStorage.getItem('SERVICE_IP'),
                                'SERVICE_IP_VALUE': localStorage.getItem('SERVICE_IP_VALUE'),
                                'device-id': localStorage.getItem('device-id'),
                                'notification-list': localStorage.getItem('notification-list'),
                            }), '*');
                        });
                        break;
                    case "cordovaFunction":
                        if (!data.action) {
                            return;
                        }
                        if (natif_1.Natif[data.action]) {
                            natif_1.Natif[data.action].apply(natif_1.Natif, data.arguments);
                        }
                        break;
                }
            });
        }
    };
});
define("configuration/configuration", ["require", "exports", "devices/devices", "configuration/mongodbModel", "configuration/oobModel", "configuration/removeActionsModel", "components/frame-messaging", "configuration/env/env"], function (require, exports, devices_1, mongodbModel_3, oobModel_1, removeActionsModel_1, frame_messaging_1, env_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    frame_messaging_1.frameMessaging.init();
    exports.env = env_2.env;
    exports.localStorage = true;
    exports.mongodb = mongodbModel_3.default;
    exports.oob = oobModel_1.default;
    exports.remoteActions = removeActionsModel_1.default;
    exports.device = devices_1.default;
});
define("components/local-notifications", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     *
     * @desc
     * 1. Create local notification component
     * 2. Check the type of the notification to send
     * 3. Send the notification
     */
    var localNotifications = /** @class */ (function () {
        function localNotifications() {
        }
        localNotifications.init = function () {
            this.registerLocalService();
        };
        localNotifications.registerLocalService = function () {
            var _this = this;
            if (typeof Cocoon !== 'undefined') {
                this.localService = Cocoon.Notification.Local;
            }
            else {
                this.localService = fakeLocalService;
            }
            this.localService.initialize({}, function (registered) {
                _this.localService.registered = registered;
            });
        };
        localNotifications.getLocalService = function () {
            return this.localService;
        };
        localNotifications.send = function (notification) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.localService.send(notification, function (err) {
                    if (err) {
                        return reject(err);
                    }
                    resolve(notification);
                });
            });
        };
        return localNotifications;
    }());
    exports.default = localNotifications;
    var fakeLocalService = {
        on: function () { return; },
        send: function () { return; },
        initialize: function (obj, cb) { return; },
    };
});
define("components/notification-entity-change-adresse", ["require", "exports", "components/notification-entity"], function (require, exports, notification_entity_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var notificationEntityChangeAdresse = /** @class */ (function (_super) {
        __extends(notificationEntityChangeAdresse, _super);
        function notificationEntityChangeAdresse() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.title = "Mise à jour de vos coordonnées";
            return _this;
        }
        notificationEntityChangeAdresse.prototype._getListHTML = function () {
            var html;
            html = '<img src="./img/alert-1-empty.png" width="100%" alt="">';
            html += "<span class=\"line-1\">" + this._formatDate(this.date) + "</span>";
            html += "<span class=\"line-2\"></span>";
            html += "<span class=\"line-3\">" + this.contentTitle + "</span>";
            html += "<span class=\"line-4\"></span>";
            html += "<span class=\"line-5\">" + this.contentBody + "</span>";
            return html;
        };
        notificationEntityChangeAdresse.prototype._getDetailHTML = function () {
            var html;
            html = "";
            if (this.userData.title) {
                html += "<strong>" + this.userData.title + "</strong><br /><br />";
            }
            for (var key in this.userData) {
                if (key === "title" || key === "type") {
                    continue;
                }
                html += "<div><span>" + key + " : </span><span class=\"light\">" + this.userData[key] + "</span></div>";
            }
            return html;
        };
        return notificationEntityChangeAdresse;
    }(notification_entity_1.default));
    exports.default = notificationEntityChangeAdresse;
});
define("components/notification-entity-credit-conso", ["require", "exports", "components/notification-entity"], function (require, exports, notification_entity_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var notificationEntityCreditConso = /** @class */ (function (_super) {
        __extends(notificationEntityCreditConso, _super);
        function notificationEntityCreditConso() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.title = "Signature de votre offre de crédit";
            return _this;
        }
        notificationEntityCreditConso.prototype._getListHTML = function () {
            var html;
            html = '<img src="./img/alert-1-empty.png" width="100%" alt="">';
            html += "<span class=\"line-1\">" + this._formatDate(this.date) + "</span>";
            html += "<span class=\"line-2\"></span>";
            html += "<span class=\"line-3\">" + this.contentTitle + "</span>";
            html += "<span class=\"line-4\"></span>";
            html += "<span class=\"line-5\">" + this.contentBody + "</span>";
            return html;
        };
        notificationEntityCreditConso.prototype._getDetailHTML = function () {
            var html;
            html = "";
            if (this.userData.title) {
                html += "<strong>" + this.userData.title + "</strong><br /><br />";
            }
            for (var key in this.userData) {
                if (key === "title" || key === "type") {
                    continue;
                }
                html += "<div><span>" + key + " : </span><span class=\"light\">" + this.userData[key] + "</span></div>";
            }
            return html;
        };
        return notificationEntityCreditConso;
    }(notification_entity_2.default));
    exports.default = notificationEntityCreditConso;
});
define("components/notification-entity-lep", ["require", "exports", "components/notification-entity"], function (require, exports, notification_entity_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var notificationEntityLEP = /** @class */ (function (_super) {
        __extends(notificationEntityLEP, _super);
        function notificationEntityLEP() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.title = "Souscription de votre Livret d'épargne populaire";
            return _this;
        }
        notificationEntityLEP.prototype._getListHTML = function () {
            var html;
            html = '<img src="./img/alert-1-empty.png" width="100%" alt="">';
            html += "<span class=\"line-1\">" + this._formatDate(this.date) + "</span>";
            html += "<span class=\"line-2\"></span>";
            html += "<span class=\"line-3\">" + this.contentTitle + "</span>";
            html += "<span class=\"line-4\"></span>";
            html += "<span class=\"line-5\">" + this.contentBody + "</span>";
            return html;
        };
        notificationEntityLEP.prototype._getDetailHTML = function () {
            var html;
            html = "";
            if (this.userData.title) {
                html += "<strong>" + this.userData.title + "</strong><br /><br />";
            }
            for (var key in this.userData) {
                if (key === "title" || key === "type") {
                    continue;
                }
                html += "<div><span>" + key + " : </span><span class=\"light\">" + this.userData[key] + "</span></div>";
            }
            return html;
        };
        return notificationEntityLEP;
    }(notification_entity_3.default));
    exports.default = notificationEntityLEP;
});
define("components/notification-factory", ["require", "exports", "components/notification-entity-change-adresse", "components/notification-entity", "components/notification-entity-credit-conso", "components/notification-entity-lep"], function (require, exports, notification_entity_change_adresse_1, notification_entity_4, notification_entity_credit_conso_1, notification_entity_lep_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * @desc generate notification types objects
     */
    var notificationFactory = /** @class */ (function () {
        function notificationFactory() {
        }
        notificationFactory.create = function (notification) {
            switch (notification.userData.type) {
                case "change-adresse":
                    return new notification_entity_change_adresse_1.default(notification);
                case "credit-conso":
                    return new notification_entity_credit_conso_1.default(notification);
                case "lep":
                    return new notification_entity_lep_1.default(notification);
                default:
                    return new notification_entity_4.default(notification);
            }
        };
        return notificationFactory;
    }());
    exports.default = notificationFactory;
});
define("components/list-notification", ["require", "exports", "configuration/configuration", "components/local-notifications", "components/dispatcher", "components/notification-factory"], function (require, exports, configuration_1, local_notifications_1, dispatcher_2, notification_factory_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var listNotification = /** @class */ (function () {
        function listNotification() {
        }
        listNotification.init = function () {
            this.restore();
            this.container = document.getElementById('notification-list');
            this.update();
        };
        listNotification.cleanStorage = function () {
            if (!this.localStorageEnabled) {
                return;
            }
            window.localStorage.setItem('notification-list', JSON.stringify([]));
        };
        listNotification.save = function () {
            if (!this.localStorageEnabled) {
                return;
            }
            window.localStorage.setItem('notification-list', JSON.stringify(this.list));
        };
        listNotification.restore = function () {
            if (!this.localStorageEnabled) {
                return;
            }
            try {
                this.list = JSON.parse(localStorage.getItem('notification-list'));
            }
            catch (e) {
                this.list = [];
                console.info(e);
            }
            if (!this.list) {
                this.list = [];
            }
        };
        listNotification.sync = function (notifications) {
            var _this = this;
            this.list = this.list.filter(function (notif) {
                var keep = false;
                notifications.forEach(function (item) {
                    if (notif.notifId === item.notifId) {
                        keep = true;
                    }
                });
                return keep;
            });
            notifications.forEach(function (notification) {
                _this.add(notification);
            });
            this.save();
            this.update();
        };
        listNotification.add = function (notification) {
            if (!this.exist(notification)) {
                local_notifications_1.default.send(notification);
                this.list.push(notification);
                this.save();
                this.update();
            }
        };
        listNotification.clear = function () {
            this.list.length = 0;
            this.save();
            this.update();
        };
        listNotification.remove = function (notification) {
            for (var index = 0; index < this.list.length; index++) {
                if (notification.notifId === this.list[index].notifId) {
                    this.list.splice(index, 1);
                    break;
                }
            }
            this.save();
            this.update();
        };
        listNotification.update = function () {
            if (this.container === null) {
                return;
                // throw new Error('no notification container available');
            }
            this.container.innerHTML = '';
            if (!this.list) {
                return;
            }
            var _loop_1 = function (notification) {
                var notif = notification_factory_1.default.create(notification);
                var notifNode = notif.toDOMNode();
                notifNode.addEventListener('click', function () {
                    dispatcher_2.default.dispatch('notification-click', notif);
                });
                this_1.container.appendChild(notifNode);
            };
            var this_1 = this;
            for (var _i = 0, _a = this.list; _i < _a.length; _i++) {
                var notification = _a[_i];
                _loop_1(notification);
            }
        };
        listNotification.exist = function (notification) {
            var bool = false;
            this.list.forEach(function (item) {
                if (item.notifId === notification.notifId) {
                    bool = true;
                }
            });
            return bool;
        };
        listNotification.list = [];
        listNotification.localStorageEnabled = configuration_1.localStorage;
        return listNotification;
    }());
    exports.default = listNotification;
});
define("components/poller", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * @desc
     * 1. Create polling
     */
    var Poller = /** @class */ (function () {
        function Poller(params) {
            this.isActive = true;
            this.options = {
                callback: function (err, response) { return; },
                timer: 3000,
            };
            this.options = __assign({}, this.options, params);
            if (isNaN(params.timer) && params.timer < 0) {
                throw new Error("Poller: timer must be a positive number");
            }
            if (typeof params.callback !== "function") {
                throw new Error("Poller: callback is not a function");
            }
            setTimeout(this.loop.bind(this), 1);
        }
        Poller.prototype.setActive = function (bool) {
            this.isActive = bool;
        };
        Poller.prototype.getActive = function () {
            return this.isActive;
        };
        Poller.prototype.loop = function () {
            var _this = this;
            // clear just in case
            clearTimeout(this.timerInstance);
            // loop all the time
            this.timerInstance = setTimeout(function () {
                _this.loop();
            }, this.options.timer);
            // run callback if active
            if (this.getActive()) {
                this.options.callback(null, null);
            }
        };
        Poller.prototype.destroy = function () {
            clearTimeout(this.timerInstance);
            this.options = null;
        };
        return Poller;
    }());
    exports.default = Poller;
});
define("components/oob-lookup", ["require", "exports", "components/poller", "configuration/configuration", "components/list-notification"], function (require, exports, poller_1, configuration_2, list_notification_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * @desc
     * 1. Create polling
     * 2. Call db to get notification list
     * 3. Remove validated notification from db collection
     */
    var oobLookup = /** @class */ (function () {
        function oobLookup() {
        }
        oobLookup.start = function (callback) {
            this.createPoller({
                timer: configuration_2.oob.polling,
                callback: callback,
            });
        };
        oobLookup.createPoller = function (params) {
            var _this = this;
            if (this.polling) {
                this.polling.destroy();
            }
            this.polling = new poller_1.default({
                callback: function () {
                    _this.polling.setActive(false);
                    _this.doRequest(function (err, result) {
                        _this.polling.setActive(true);
                        params.callback(err, result);
                    });
                },
                timer: params.timer,
            });
        };
        oobLookup.remove = function (notification) {
            fetch(this.uri.replace(":id", notification.notifId), {
                method: "DELETE",
            });
            list_notification_1.default.remove(notification);
        };
        oobLookup.doRequest = function (callback) {
            fetch(this.uri.replace(":id", ""))
                .then(function (response) { return response.json(); })
                .then(function (response) { return response._embedded ? response._embedded : response; })
                .then(function (response) {
                return response.filter(function (item) {
                    return item.target === configuration_2.device.getTarget().devices.mobile;
                });
            })
                .then(function (response) {
                return response.map(function (item) {
                    // mongolab
                    if (item._id.$oid) {
                        item.notifId = item._id.$oid.toString();
                        // default
                    }
                    else if (item._id) {
                        item.notifId = item._id.toString();
                    }
                    return item;
                });
            })
                .then(function (response) { callback(null, response); })
                .catch(function (err) {
                callback(err, null);
            });
        };
        oobLookup.uri = configuration_2.oob.uri;
        return oobLookup;
    }());
    exports.default = oobLookup;
});
define("components/remote-actions", ["require", "exports", "components/poller", "configuration/configuration", "components/local-notifications"], function (require, exports, poller_2, configuration_3, local_notifications_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     * @desc
     * 1. Create poller to remote-actions db collection
     * 2. Run action when received one
     * 3. Remove action from the remote-actions db collection
     */
    var remoteActions = /** @class */ (function () {
        function remoteActions() {
        }
        remoteActions.init = function () {
            this.createPoller();
        };
        remoteActions.createPoller = function () {
            var _this = this;
            if (this.polling) {
                this.polling.destroy();
            }
            this.polling = new poller_2.default({
                callback: function () {
                    _this.polling.setActive(false);
                    fetch(_this.uri.replace(':id', ''))
                        .then(function (response) { return response.json(); })
                        .then(function (response) { return response._embedded ? response._embedded : response; })
                        .then(function (response) {
                        return response.filter(function (item) {
                            return item.target === configuration_3.device.getTarget().devices.mobile;
                        });
                    })
                        .then(function (response) {
                        // iterate on all retrieved actions
                        response.forEach(function (item) {
                            var id;
                            // mongolab
                            if (item._id.$oid) {
                                id = item._id.$oid.toString();
                                // default
                            }
                            else if (item._id) {
                                id = item._id.toString();
                            }
                            // remove action from db collection
                            fetch(_this.uri.replace(':id', id), {
                                method: 'DELETE',
                            });
                            _this.doAction(item.type, item.data);
                        });
                        _this.polling.setActive(true);
                    })
                        .catch(function (err) {
                        _this.polling.setActive(true);
                    });
                },
                timer: configuration_3.remoteActions.polling,
            });
        };
        remoteActions.doAction = function (type, data) {
            console.log('do action', type, data);
            switch (type) {
                case 'openWindow':
                    cordova.InAppBrowser.open(data.url, '_system');
                    break;
                case 'localNotification':
                    data.notification.userData.moveToBackground =
                        typeof data.notification.userData.moveToBackground !== 'undefined' ?
                            data.notification.userData.moveToBackground : true;
                    setTimeout(function () {
                        local_notifications_2.default.send(data.notification);
                    }, data.notification.userData.delay || 1000);
                    break;
            }
        };
        remoteActions.sendAction = function (data) {
            return fetch(remoteActions.uri.replace(':id', ''), {
                body: JSON.stringify(data),
                headers: [
                    ['Accept', 'application/json'],
                    ['Content-Type', 'application/json'],
                ],
                method: 'POST',
            })
                .then(function (response) { return response.json(); });
        };
        remoteActions.uri = configuration_3.remoteActions.uri;
        return remoteActions;
    }());
    exports.default = remoteActions;
});
define("index", ["require", "exports", "components/state-manager", "components/list-notification", "components/local-notifications", "components/oob-lookup", "components/dispatcher", "components/remote-actions", "components/notification-factory", "components/natif"], function (require, exports, state_manager_1, list_notification_2, local_notifications_3, oob_lookup_1, dispatcher_3, remote_actions_1, notification_factory_2, natif_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var app = {
        // Application Constructor
        initialize: function () {
            document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
            setTimeout(function () {
                if (typeof Cocoon === 'undefined') {
                    dispatcher_3.default.dispatch('deviceready');
                }
            }, 1000);
        },
        // deviceready Event Handler
        //
        // Bind any cordova events here. Common events are:
        // 'pause', 'resume', etc.
        onDeviceReady: function () {
            this.receivedEvent('deviceready');
            list_notification_2.default.init();
            state_manager_1.default.init();
            local_notifications_3.default.init();
            remote_actions_1.default.init();
            natif_2.Natif.enableBackgroundMode(true);
            this.startOobLookup();
            document.body.classList.remove('hide');
            local_notifications_3.default.getLocalService().on('notification', function (userData) {
                if (userData.moveToBackground === true) {
                    natif_2.Natif.moveToBackground();
                }
                if (userData.moveToForground === true) {
                    natif_2.Natif.moveToForground();
                }
                switch (userData.redirectTo) {
                    case undefined:
                    case 'oob':
                        if (location.href.indexOf('oob') === -1) {
                            location.href = '/android_asset/www/oob/index.html';
                        }
                        break;
                    case 'alerting_detail':
                        if (location.href.indexOf('alerting_detail') === -1) {
                            location.href = '/android_asset/www/alerting_detail/index.html';
                        }
                        break;
                    case 'alerting':
                        if (location.href.indexOf('alerting') === -1) {
                            location.href = '/android_asset/www/alerting/index.html';
                        }
                        break;
                    case 'alerting/detailAlertAction':
                        if (location.href.indexOf('alerting/detailAlertAction') === -1) {
                            location.href = '/android_asset/www/alerting/index.html?state=detailAlerteAction';
                        }
                        break;
                }
            });
        },
        // Update DOM on a Received Event
        receivedEvent: function (id) {
            console.info('Received Event: ' + id);
        },
        startOobLookup: function () {
            dispatcher_3.default.dispatch('loading-start');
            oob_lookup_1.default.start(function (err, notifications) {
                dispatcher_3.default.dispatch('loading-end');
                if (err) {
                    return;
                }
                notifications = notifications.map(function (item) {
                    return notification_factory_2.default.create(item);
                });
                list_notification_2.default.sync(notifications);
            });
        },
    };
    app.initialize();
});
define("modules/alerting/index", ["require", "exports", "configuration/env/env", "components/natif", "components/dispatcher", "index"], function (require, exports, env_3, natif_3, dispatcher_4) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    console.info('alerting OK');
    var app = document.querySelector('.app');
    dispatcher_4.default.register('deviceready', natif_3.Natif.lockPortraitScreenOrientation);
    var ALERTING_URI = env_3.env.alerting_URL;
    var URI_Fragments = location.href.split('state=');
    var IFRAME_URI;
    if (URI_Fragments[1]) {
        IFRAME_URI = env_3.env.alerting_URL + URI_Fragments[1] + '.html';
    }
    else {
        IFRAME_URI = ALERTING_URI + 'parametre.html';
    }
    app.innerHTML =
        "<iframe class=\"full-page-iframe\" frameborder=\"0\" src=\"" + IFRAME_URI + "\" width=\"100%\" height=\"100%\" />";
});
//# sourceMappingURL=main.js.map