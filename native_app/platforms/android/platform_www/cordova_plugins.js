cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cocoon-plugin-common.Cocoon",
    "file": "plugins/cocoon-plugin-common/www/cocoon.js",
    "pluginId": "cocoon-plugin-common",
    "runs": true
  },
  {
    "id": "cocoon-plugin-notifications-common.Notifications",
    "file": "plugins/cocoon-plugin-notifications-common/www/cocoon_notifications.js",
    "pluginId": "cocoon-plugin-notifications-common",
    "runs": true
  },
  {
    "id": "cocoon-plugin-notifications-android-local.Notifications",
    "file": "plugins/cocoon-plugin-notifications-android-local/www/cocoon_notifications_local.js",
    "pluginId": "cocoon-plugin-notifications-android-local",
    "runs": true
  },
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-background-mode.BackgroundMode",
    "file": "plugins/cordova-plugin-background-mode/www/background-mode.js",
    "pluginId": "cordova-plugin-background-mode",
    "clobbers": [
      "cordova.plugins.backgroundMode",
      "plugin.backgroundMode"
    ]
  },
  {
    "id": "cordova-plugin-inappbrowser.inappbrowser",
    "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
    "pluginId": "cordova-plugin-inappbrowser",
    "clobbers": [
      "cordova.InAppBrowser.open",
      "window.open"
    ]
  },
  {
    "id": "cordova-plugin-vibration.notification",
    "file": "plugins/cordova-plugin-vibration/www/vibration.js",
    "pluginId": "cordova-plugin-vibration",
    "merges": [
      "navigator.notification",
      "navigator"
    ]
  },
  {
    "id": "es6-promise-plugin.Promise",
    "file": "plugins/es6-promise-plugin/www/promise.js",
    "pluginId": "es6-promise-plugin",
    "runs": true
  },
  {
    "id": "cordova-plugin-screen-orientation.screenorientation",
    "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.js",
    "pluginId": "cordova-plugin-screen-orientation",
    "clobbers": [
      "cordova.plugins.screenorientation"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-whitelist": "1.3.3",
  "cocoon-plugin-common": "1.0.2",
  "cocoon-plugin-notifications-common": "1.0.6",
  "cocoon-plugin-notifications-android-local": "1.0.4",
  "cordova-plugin-device": "1.1.7",
  "cordova-plugin-background-mode": "0.7.2",
  "cordova-plugin-inappbrowser": "1.7.2",
  "cordova-plugin-vibration": "2.1.6",
  "es6-promise-plugin": "4.1.0",
  "cordova-plugin-screen-orientation": "2.0.2"
};
// BOTTOM OF METADATA
});