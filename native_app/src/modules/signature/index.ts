import '../../index';
import { env } from '../../configuration/env/env';
import { Natif } from '../../components/natif';
import dispatcher from '../../components/dispatcher';

console.info('signature OK');

const app = document.querySelector('.app');

dispatcher.register('deviceready', Natif.lockPortraitScreenOrientation);

app.innerHTML = `<iframe class="full-page-iframe" frameborder="0" src="${env.signature_URL}" width="100%" height="100%" />`;