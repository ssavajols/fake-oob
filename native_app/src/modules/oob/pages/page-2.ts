import stateManager from '../../../components/state-manager';
import dispatcher from '../../../components/dispatcher';

let notification;

const accepterBtn: any = document.querySelector('.btn-accepter');
const refuserBtn: any = document.querySelector('.btn-refuser');

const keyboard: any = document.querySelector('.keyboard');
const keyboardNumbers: any = document.querySelector('.keyboard-numbers');
const keyboardConfirmBtn: any = document.querySelector('.keyboard-confirm-btn');
const keyboardCancelBtn: any = document.querySelector('.keyboard-cancel-btn');
const keyPushed: any = document.querySelector('.key-pushed');

const notificationDetailContainer: any = document.querySelector('.notification-detail-container');
const notificationDetailTitle: any = document.querySelector('.notification-detail-title');

accepterBtn.addEventListener('click', showKeyboard);
refuserBtn.addEventListener('click', goBack);

keyboardConfirmBtn.addEventListener('click', goNext);
keyboardCancelBtn.addEventListener('click', hideKeyboard);
keyboardNumbers.addEventListener('click', keyboardPress);

dispatcher.register('page-2-show', (event) =>
{
    keyboard.classList.remove('show-from-bottom');
    keyPushed.value = '';
    notification = event.data;
    notificationDetailContainer.innerHTML = notification.toHTML('detail');
    notificationDetailTitle.innerHTML = notification.getTitle();

});

function keyboardPress()
{
    if ( keyPushed.value.length < 6 ) {
        keyPushed.value += '1';
    }
}

function hideKeyboard()
{
    keyboard.classList.remove('show-from-bottom');
    keyPushed.value = '';
}

function showKeyboard()
{
    keyboard.classList.add('show-from-bottom');
}

function goNext(){
    if ( keyPushed.value.length > 5)
    {
        stateManager.show('3', notification);
    }
}

function goBack(){
    stateManager.goBack(1);
}