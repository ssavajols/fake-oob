import stateManager from '../../../components/state-manager';

const homeBtn: Element = document.querySelector('.page-4 .btn-home');

homeBtn.addEventListener('click', goHome);

function goHome()
{
    stateManager.show('1');
    stateManager.clear();
}