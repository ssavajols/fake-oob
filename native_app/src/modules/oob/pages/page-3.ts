import dispatcher from '../../../components/dispatcher';
import oobLookup from '../../../components/oob-lookup';
import stateManager from '../../../components/state-manager';

let notification = null;

const homeBtn: Element = document.querySelector('.page-3 .btn-home');

homeBtn.addEventListener('click', goHome);

dispatcher.register('page-3-show', (event: Event) =>
{
    if ( !event.data ) {
        return;
    }

    notification = event.data;

    oobLookup.remove(notification);
});

function goHome()
{
    stateManager.show('1');
    stateManager.clear();
}
