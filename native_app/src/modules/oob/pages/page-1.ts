import dispatcher from '../../../components/dispatcher';
import stateManager from '../../../components/state-manager';

dispatcher.register('page-1-show', () =>
{
});

dispatcher.register('notification-click', (event) =>
{
    stateManager.show('2', event.data);
});
