import * as configuration from '../../configuration/configuration';

import * as dispatcher from '../../components/dispatcher';
import * as Poller from '../../components/poller';
import * as backdoor from '../../components/backdoor';
import * as remoteActions from '../../components/remote-actions';
import * as notificationEntity from '../../components/notification-entity';
import * as localNotifications from '../../components/local-notifications';
import * as listNotification from '../../components/list-notification';
import * as stateManager from '../../components/state-manager';
import * as oobLookup from '../../components/oob-lookup';

import '../../components/header';
import '../../components/content';

import './pages/page-1';
import './pages/page-2';
import './pages/page-3';
import './pages/page-4';

import '../../index';
import { Natif } from '../../components/natif';

dispatcher.default.register('deviceready', Natif.lockPortraitScreenOrientation);

export const OOB = {
    Poller,
    backdoor,
    configuration,
    dispatcher,
    listNotification,
    localNotifications,
    notificationEntity,
    oobLookup,
    remoteActions,
    stateManager,
};

window.OOB = OOB;