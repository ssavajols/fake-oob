import { Authent } from './../../components/authent';
import '../../index';
import { Natif } from '../../components/natif';
import dispatcher from '../../components/dispatcher';

Authent.create();

dispatcher.register('deviceready', Natif.lockPortraitScreenOrientation);

console.info('alerting detail OK');