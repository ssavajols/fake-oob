import '../../index';
import { Natif } from 'src/components/natif';
import dispatcher from 'src/components/dispatcher';


dispatcher.register('deviceready', Natif.lockPortraitScreenOrientation);