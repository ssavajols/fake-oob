import '../../index';
import { env } from '../../configuration/env/env';
import { Natif } from '../../components/natif';
import dispatcher from '../../components/dispatcher';

console.info('alerting OK');

const app = document.querySelector('.app');

dispatcher.register('deviceready', Natif.lockPortraitScreenOrientation);

const ALERTING_URI = env.alerting_URL;
const URI_Fragments = location.href.split('state=');

let IFRAME_URI;

if( URI_Fragments[1] ) {
    IFRAME_URI = env.alerting_URL + URI_Fragments[1] + '.html';
} else {
    IFRAME_URI = ALERTING_URI + 'parametre.html';
}

app.innerHTML =
    `<iframe class="full-page-iframe" frameborder="0" src="${IFRAME_URI}" width="100%" height="100%" />`;