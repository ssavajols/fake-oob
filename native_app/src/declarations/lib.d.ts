interface Window {
    OOB: any;
    cordova: any;
}

interface Event {
    data?: any;
}

interface Screen {
    orientation: CordovaOrientation;
}

interface CordovaOrientation {
    unlock: () => Promise<any>;
    lock: (orientation: string) => Promise<any>;
}

interface PollerOptions {
    timer: number;
    callback: callback;
}

interface callback extends Function {
    (err: Error, response?: any);
}

interface CocoonInterface {
    Notification: any;
}

interface CordovaInterface {
    InAppBrowser: any;
    plugins: any;
}

declare const Promise: any;
declare const Cocoon: CocoonInterface;
declare const cordova: CordovaInterface;

type DB_TYPE = "mongolab" | "mongodb-rest";