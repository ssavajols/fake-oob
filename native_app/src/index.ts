import stateManager from './components/state-manager';
import listNotification from './components/list-notification';
import localNotifications from './components/local-notifications';
import oobLookup from './components/oob-lookup';
import dispatcher from './components/dispatcher';
import remoteActions from './components/remote-actions';
import notificationFactory from './components/notification-factory';
import { Natif } from './components/natif';

const app = {
    // Application Constructor
    initialize() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        setTimeout(() =>
        {
            if ( typeof Cocoon === 'undefined' )
            {
                dispatcher.dispatch('deviceready');
            }
        }, 1000);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady() {
        this.receivedEvent('deviceready');

        listNotification.init();
        stateManager.init();
        localNotifications.init();
        remoteActions.init();

        Natif.enableBackgroundMode(true);

        this.startOobLookup();

        document.body.classList.remove('hide');

        localNotifications.getLocalService().on('notification', (userData) => {
            if ( userData.moveToBackground === true )
            {
                Natif.moveToBackground();
            } 
            
            if ( userData.moveToForground === true )
            {
                Natif.moveToForground();
            }

            switch ( userData.redirectTo ) {
                case undefined :
                case 'oob':
                    if (location.href.indexOf('oob') === -1 ) {
                        location.href = '/android_asset/www/oob/index.html';
                    }
                    break;
                case 'alerting_detail':
                    if (location.href.indexOf('alerting_detail') === -1 ) {
                        location.href = '/android_asset/www/alerting_detail/index.html';
                    }
                    break;
                case 'alerting':
                    if (location.href.indexOf('alerting') === -1 ) {
                        location.href = '/android_asset/www/alerting/index.html';
                    }
                    break;
                case 'alerting/detailAlertAction':
                    if (location.href.indexOf('alerting/detailAlertAction') === -1 ) {
                        location.href = '/android_asset/www/alerting/index.html?state=detailAlerteAction';
                    }
                    break;
            }

        });
    },

    // Update DOM on a Received Event
    receivedEvent(id) {
        console.info('Received Event: ' + id);
    },

    startOobLookup() {

        dispatcher.dispatch('loading-start');
        oobLookup.start((err, notifications) =>
        {
            dispatcher.dispatch('loading-end');
            if ( err )  {
                return;
            }

            notifications = notifications.map((item) =>
            {
                return notificationFactory.create(item);
            });

            listNotification.sync(notifications);

        });
    },

};

app.initialize();