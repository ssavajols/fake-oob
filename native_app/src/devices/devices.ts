import deviceModel from "./deviceModel";

export default class devices{
    public static list: deviceModel[] = [
        new deviceModel("show-room-group-1", {
            agence: "show-room-group-1-desktop-3",
            back: "show-room-group-1-desktop-2",
            front: "show-room-group-1-desktop-1",
            mobile: "show-room-group-1-mobile-1",
        }),
        new deviceModel("show-room-group-2", {
            agence: "show-room-group-2-desktop-3",
            back: "show-room-group-2-desktop-2",
            front: "show-room-group-2-desktop-1",
            mobile: "show-room-group-2-mobile-1",
        }),
        new deviceModel("show-room-group-3", {
            agence: "show-room-group-3-desktop-3",
            back: "show-room-group-3-desktop-2",
            front: "show-room-group-3-desktop-1",
            mobile: "show-room-group-3-mobile-1",
        }),
        new deviceModel("show-room-group-4", {
            agence: "show-room-group-4-desktop-3",
            back: "show-room-group-4-desktop-2",
            front: "show-room-group-4-desktop-1",
            mobile: "show-room-group-4-mobile-1",
        }),
        new deviceModel("show-room-group-5", {
            agence: "show-room-group-5-desktop-3",
            back: "show-room-group-5-desktop-2",
            front: "show-room-group-5-desktop-1",
            mobile: "show-room-group-5-mobile-1",
        }),
        new deviceModel("show-room-group-6", {
            agence: "show-room-group-6-desktop-3",
            back: "show-room-group-6-desktop-2",
            front: "show-room-group-6-desktop-1",
            mobile: "show-room-group-6-mobile-1",
        }),
        new deviceModel("show-room-group-7", {
            agence: "show-room-group-7-desktop-3",
            back: "show-room-group-7-desktop-2",
            front: "show-room-group-7-desktop-1",
            mobile: "show-room-group-7-mobile-1",
        }),
        new deviceModel("show-room-group-8", {
            agence: "show-room-group-8-desktop-3",
            back: "show-room-group-8-desktop-2",
            front: "show-room-group-8-desktop-1",
            mobile: "show-room-group-8-mobile-1",
        }),
        new deviceModel("show-room-group-9", {
            agence: "show-room-group-9-desktop-3",
            back: "show-room-group-9-desktop-2",
            front: "show-room-group-9-desktop-1",
            mobile: "show-room-group-9-mobile-1",
        }),
        new deviceModel("stand-group-1", {
            agence: "stand-group-1-desktop-3",
            back: "stand-group-1-desktop-2",
            front: "stand-group-1-desktop-1",
            mobile: "stand-group-1-mobile-1",
        }),
        new deviceModel("stand-group-2", {
            agence: "stand-group-2-desktop-3",
            back: "stand-group-2-desktop-2",
            front: "stand-group-2-desktop-1",
            mobile: "stand-group-2-mobile-1",
        }),
        new deviceModel("stand-group-3", {
            agence: "stand-group-3-desktop-3",
            back: "stand-group-3-desktop-2",
            front: "stand-group-3-desktop-1",
            mobile: "stand-group-3-mobile-1",
        }),
        new deviceModel("stand-group-4", {
            agence: "stand-group-4-desktop-3",
            back: "stand-group-4-desktop-2",
            front: "stand-group-4-desktop-1",
            mobile: "stand-group-4-mobile-1",
        }),
        new deviceModel("stand-group-5", {
            agence: "stand-group-5-desktop-3",
            back: "stand-group-5-desktop-2",
            front: "stand-group-5-desktop-1",
            mobile: "stand-group-5-mobile-1",
        }),
        new deviceModel("stand-group-6", {
            agence: "stand-group-6-desktop-3",
            back: "stand-group-6-desktop-2",
            front: "stand-group-6-desktop-1",
            mobile: "stand-group-6-mobile-1",
        }),
        new deviceModel("stand-group-7", {
            agence: "stand-group-7-desktop-3",
            back: "stand-group-7-desktop-2",
            front: "stand-group-7-desktop-1",
            mobile: "stand-group-7-mobile-1",
        }),
        new deviceModel("stand-group-8", {
            agence: "stand-group-8-desktop-3",
            back: "stand-group-8-desktop-2",
            front: "stand-group-8-desktop-1",
            mobile: "stand-group-8-mobile-1",
        }),
        new deviceModel("stand-group-9", {
            agence: "stand-group-9-desktop-3",
            back: "stand-group-9-desktop-2",
            front: "stand-group-9-desktop-1",
            mobile: "stand-group-9-mobile-1",
        }),
    ];

    public static getTarget(): deviceModel
    {
        let device;

        if ( window.localStorage.getItem("device-id") === null )
        {
            devices.setTarget(JSON.stringify(devices.list[0]));
        }

        try {
            const parsed = JSON.parse(window.localStorage.getItem("device-id"));
            device = new deviceModel(parsed.name, parsed.devices);
        }catch (e){
            console.info("no valid device configuration in localStorage");
        }
        return device;
    }

    public static setTarget(target: string): string
    {
        window.localStorage.setItem("device-id", JSON.stringify(target));
        return target;
    }

    public static getConfigurationByName(name: string): deviceModel|null
    {
        let config: deviceModel|null = null;

        devices.list.forEach((item) =>
         {
             if ( item.name === name )
             {
                 config = item;
             }
         });

        return config;
    }
}