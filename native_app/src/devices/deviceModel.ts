export default class deviceModel {
    public name: string;
    public devices: any;
    constructor(name: string, devices: Object){
        this.name = name;
        this.devices = devices;
    }
}