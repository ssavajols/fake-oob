import envDawtlx05 from './env.dawtlx05';
import envLocal from './env.local';
import envCustom from './env.custom';

export const dawtlx05 = envDawtlx05;
export const local = envLocal;
export const custom = envCustom;