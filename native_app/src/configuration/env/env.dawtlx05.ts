// import "../../declarations/lib";

const SERVICE_IP_VALUE = localStorage.getItem('SERVICE_IP_VALUE');

export default class dawtlx05
{
    public static db_host: string = 'http://awt.bddf.applis.bad.socgen/show-room-mongo-rest/db/:collection/:id';
    public static db_type: DB_TYPE = 'mongodb-rest';
    public static static_server: string = "http://localhost:8080";
    public static signature_URL: string = `http://awt.bddf.applis.bad.socgen/show-room-statics/fake_signature_mobile/signature/mesDemandesAccueil.html`;
    public static alerting_URL: string = `http://awt.bddf.applis.bad.socgen/show-room-statics/alerting/`;
}