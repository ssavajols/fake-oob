import * as envConfig from './index';

let _env; 

switch (window.localStorage.getItem('SERVICE_IP')) {
    case 'local':
        _env = envConfig.local;
        break;
    case 'custom':
        _env = envConfig.custom;
        break;
    default:
        _env = envConfig.dawtlx05;
}

export const env = _env;