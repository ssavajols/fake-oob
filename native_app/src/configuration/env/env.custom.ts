// import '../../declarations/lib';

const SERVICE_IP_VALUE = localStorage.getItem('SERVICE_IP_VALUE');

export default class local
{
    public static db_host: string = `http://${SERVICE_IP_VALUE}:27023/db/:collection/:id`;
    public static db_type: DB_TYPE = 'mongodb-rest';
    public static static_server: string = `http://${SERVICE_IP_VALUE}:8080`;
    public static signature_URL: string = `http://${SERVICE_IP_VALUE}:8080`;
    public static alerting_URL: string = `http://${SERVICE_IP_VALUE}:8080`;
}