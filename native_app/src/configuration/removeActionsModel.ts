import mongodbModel from "./mongodbModel";

export default class remoteActionsModel {
    public static polling: number    = 3000;
    public static uri: string        = mongodbModel.getUri("remote_actions", ":id");
}