import devices from "../devices/devices";
import mongodbModel from "./mongodbModel";
import oobModel from "./oobModel";
import remoteActionsModel from "./removeActionsModel";
import { frameMessaging } from "./../components/frame-messaging";

frameMessaging.init();

export { env } from './env/env';
export const localStorage: boolean = true;
export const mongodb = mongodbModel;
export const oob = oobModel;
export const remoteActions = remoteActionsModel;
export const device = devices;