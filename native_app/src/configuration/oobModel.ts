import mongodbModel from "./mongodbModel";

export default class oobModel {
    public static polling: number  = 3000;
    public static uri: string      = mongodbModel.getUri("oob", ":id");
}