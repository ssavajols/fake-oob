import { env } from './env/env';

export default class mongodbModel {
    public static getUri(collection: string, id: string) {
        const hostname: string = env.db_host;
        const type: DB_TYPE = env.db_type;

        if (type === 'mongolab' || type === 'mongodb-rest') {
            return hostname.replace(':collection', collection).replace(':id', id);
        }
        return '';
    }
}