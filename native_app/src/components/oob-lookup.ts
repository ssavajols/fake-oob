import Poller from "./poller";
import {device, oob as oobModel} from "../configuration/configuration";
import notificationEntity from "./notification-entity";
import listNotification from "./list-notification";

interface oobLookupOptions {
    callback: callback;
    timer: number;
}

/**
 * @desc
 * 1. Create polling
 * 2. Call db to get notification list
 * 3. Remove validated notification from db collection
 */
export default class oobLookup {
    private static polling: Poller;
    private static uri: string = oobModel.uri;

    public static start(callback: callback): void
    {
        this.createPoller({
            timer: oobModel.polling,
            callback,
        });
    }

    public static createPoller(params: any): void
    {
        if ( this.polling )
        {
            this.polling.destroy();
        }

        this.polling = new Poller({
            callback: () =>
            {
                this.polling.setActive(false);
                this.doRequest((err, result) =>
                {
                    this.polling.setActive(true);
                    params.callback(err, result);
                });

            },
            timer: params.timer,
        });
    }

    public static remove(notification: notificationEntity): void
    {
        fetch(this.uri.replace(":id", notification.notifId), {
            method: "DELETE",
        });

        listNotification.remove(notification);
    }

    public static doRequest(callback: callback): void
    {
        fetch(this.uri.replace(":id", ""))
        // create json response
            .then((response) => response.json())
            .then((response) => response._embedded ? response._embedded : response)
            // filter targets
            .then((response: any) =>
            {
                return response.filter((item) =>
                {
                    return item.target === device.getTarget().devices.mobile;
                });
            })
            // set notification id
            .then((response) =>
            {
                return response.map((item) =>
                {
                    // mongolab
                    if ( item._id.$oid )
                    {
                        item.notifId = item._id.$oid.toString();

                        // default
                    }else if ( item._id )
                    {
                        item.notifId = item._id.toString();
                    }

                    return item;
                });
            })
            // run callback
            .then((response) => { callback(null, response); })
            .catch((err) =>
            {
                callback(err, null);
            });
    }
}