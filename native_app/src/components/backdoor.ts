import dispatcher from './dispatcher';
import {device} from '../configuration/configuration';

let count = 0;
const configurationPanel = document.querySelector('.configuration-panel');
const deviceList = configurationPanel.querySelector('.device-list');
const closeBtn = document.querySelector('.configuration-panel > .close');
const serviceIpForm = document.getElementById('service-ip-form');
const service_ip_inputs: NodeListOf<HTMLInputElement> = document.querySelectorAll('[name=service-ip]');
const service_ip_value_input: HTMLInputElement = document.querySelector('[name=service-ip-value]');
let timer = null;

createDeviceList(deviceList, device.list);

if ( serviceIpForm ) {
    selectServiceIp(localStorage.getItem('SERVICE_IP'), localStorage.getItem('SERVICE_IP_VALUE'));
    serviceIpAddListener();
}

closeBtn.addEventListener('click', () =>
{
    configurationPanel.classList.remove('show');
});

document.querySelector('.header').addEventListener('touchstart', () =>
{
    count++;

    if ( !timer )
    {
        timer = setTimeout(() =>
        {
            clearTimer();
        }, 1250);
    }

    if ( count > 4 )
    {
        clearTimer();
        dispatcher.dispatch('five-taps');
    }
});

dispatcher.register('five-taps', () =>
{
    configurationPanel.classList.add('show');
});

dispatcher.register('set-device-id', (event: any) =>
{
    device.setTarget(event.data);
});

function clearTimer()
{
    clearTimeout(timer);
    timer = null;
    count = 0;
}

function serviceIpAddListener(){
    serviceIpForm.addEventListener('submit', (event: any) => {

        Array.prototype.forEach.call(service_ip_inputs, (input: HTMLInputElement) => {
            if ( input.checked ) {
                localStorage.setItem('SERVICE_IP', input.value);

                if ( input.value === 'custom') {
                    localStorage.setItem('SERVICE_IP_VALUE', service_ip_value_input.value);
                }
            }
        });
        event.preventDefault();

        setTimeout(() => window.location.reload(), 100);
    });
}

function selectServiceIp(service_ip, service_ip_value) {

    if ( !service_ip_inputs.length ) {
        return;
    }

    Array.prototype.forEach.call(service_ip_inputs, (input: HTMLInputElement) => {
        if ( input.value === service_ip ) {
            input.checked = true;
        }
    });

    if ( service_ip === 'custom' ) {
        service_ip_value_input.value = service_ip_value;
    }

    if ( !service_ip ) {
        service_ip_inputs[0].checked = true;
    }
}

function createDeviceList(container, list)
{
    let html = '';

    list.forEach((item) =>
    {
        html += '<div>';
        if ( device.getTarget().name === item.name )
        {
            html += `<input type='radio' id='device-${item.name}' name='device-id' value='${item.name}' checked />`;
        }else
        {
            html += `<input type='radio' id='device-${item.name}' name='device-id' value='${item.name}' />`;
        }

        html += `<label for='device-${item.name}'>`;
        html += ` ${item.name}`;
        html += '</label>';
        html += '</div>';
    });

    container.innerHTML = html;

    setTimeout(() =>
    {
        const elements = container.querySelectorAll('input');
        for (const element of elements )
        {
            element.addEventListener('change', (event) =>
            {
                dispatcher.dispatch('set-device-id', device.getConfigurationByName(event.srcElement.value));
            });
        }
    }, 0);
}
