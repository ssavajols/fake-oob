import notificationEntity from './notification-entity';
/**
 *
 * @desc
 * 1. Create local notification component
 * 2. Check the type of the notification to send
 * 3. Send the notification
 */
export default class localNotifications {

    private static localService: any;

    public static init()
    {
        this.registerLocalService();
    }

    public static registerLocalService()
    {
        if ( typeof Cocoon !== 'undefined' )
        {
            this.localService = Cocoon.Notification.Local;
        }else
        {
            this.localService = fakeLocalService;
        }

        this.localService.initialize({}, (registered) => {
            this.localService.registered = registered;
        });
    }

    public static getLocalService(): any
    {
        return this.localService;
    }

    public static send(notification: notificationEntity): any
    {
        return new Promise((resolve, reject) =>
        {

            this.localService.send(notification, (err) =>
            {
                if ( err ) {
                    return reject(err);
                }

                resolve(notification);
            });

        });
    }
}

const fakeLocalService = {
    on(): void { return; },
    send(): void { return; },
    initialize(obj: object, cb: () => void): void { return; },
};