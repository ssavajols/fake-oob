import notificationEntityChangeAdresse from "./notification-entity-change-adresse";
import notificationEntity from "./notification-entity";
import notificationEntityCreditConso from "./notification-entity-credit-conso";
import notificationEntityLEP from "./notification-entity-lep";

/**
 * @desc generate notification types objects
 */
export default class notificationFactory {
    public static create(notification: any): notificationEntity
    {
        switch ( notification.userData.type )
        {
            case "change-adresse":
                return new notificationEntityChangeAdresse( notification );

            case "credit-conso":
                return new notificationEntityCreditConso( notification );

            case "lep":
                return new notificationEntityLEP( notification );

            default:
                return new notificationEntity( notification );
        }
    }
}