import dispatcher from "./dispatcher";

const content: any = document.querySelector(".content");
const header: any = document.querySelector(".header");

document.addEventListener("resize", () =>
{
    const minHeight = window.innerHeight - header.clientHeight;

    content.style.position = "relative";
    content.style.height = `${minHeight}px`;
    content.style.top = `${header.height}px`;
});

setInterval(() =>
{
    dispatcher.dispatch("resize");
}, 250);