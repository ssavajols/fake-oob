/**
 * @desc
 * 1. Manage state application
 * 2. Manage navigation history
 * @type {{}}
 */
import dispatcher from "src/components/dispatcher";

export default class stateManager {
    private static states: any;
    private static history: any[] = [];
    private static currentState: any;

    public static init(): void
    {
        stateManager.getStates();

        if ( this.states[0] )
        {
            stateManager.show(this.states[0].dataset.state);
        }
    }

    public static clear(): void
    {
        this.history.length = 0;
    }

    public static goBack(n: number): void
    {
        let state = null;

        if ( !n ) {
            n = 1;
        }

        while (n > 0)
        {
            state = this.history.pop();

            // skip current state
            if (state && state.id === this.currentState.id ) {
                continue;
            }

            n--;
        }

        if ( state ) {
            stateManager.show(state.id, state.data, true);
        }
    }

    public static show(id: string, data?: any, ignoreHistory?: boolean): void
    {
        if ( !ignoreHistory ) {
            this.history.push(this.currentState);
        }

        for (const state of this.states)
        {
            if ( state.dataset.state === id )
            {
                dispatcher.dispatch(`page-${state.dataset.state}-show`, data);

                this.currentState = {id, data};
                state.classList.add("show");
                state.classList.remove("hide");

            }else
            {
                dispatcher.dispatch(`page-${state.dataset.state}-hide`, data);
                state.classList.add("hide");
                state.classList.remove("show");
            }
        }
    }

    public static getStates()
    {
        this.states = document.querySelectorAll("[data-state]");
    }
}