export class Natif {

    public static moveToBackground() {
        if ( typeof cordova !== 'undefined' ) {
            cordova.plugins.backgroundMode.moveToBackground();
        }
    }

    public static moveToForground() {
        if ( typeof cordova !== 'undefined' ) {
            cordova.plugins.backgroundMode.moveToForground();
        }
    }

    public static enableBackgroundMode(active: boolean) {
        if ( typeof cordova !== 'undefined' ) {
            cordova.plugins.backgroundMode.setEnabled(active);
        }
    }

    public static unlockScreenOrientation() {
        if ( typeof cordova !== 'undefined' ) {
            screen.orientation.unlock();
        }
    }

    public static lockPortraitScreenOrientation() {

        if ( typeof cordova !== 'undefined' ) {
            screen.orientation.lock('portrait');            
        }
    }

    public static lockLandscapeScreenOrientation() {

        if ( typeof cordova !== 'undefined' ) {
            screen.orientation.lock('landscape');            
        }
    }

}