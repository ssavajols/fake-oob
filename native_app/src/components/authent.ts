export class Authent {

    private static isInitialized = false;
    private static authentContainer;
    private static keyboard;
    private static valid_authent;
    private static password;

    public static create(): Promise<void> {
        document.body.innerHTML += `
        <div class="authent">
            <img src="./img/authent/authent-1.png" width="100%" alt="" />
            <img src="./img/authent/authent-2.png" class="authent_keyboard" width="100%" alt="" />
            <img src="./img/authent/authent-3.png" class="valid_authent" width="100%" alt="" />
            <input type="password" class="password" name="" disabled />
        </div>`;

        return new Promise((resolve) => {
            setTimeout(() => {
                Authent.initialize();
                resolve();
            }, 1);
        });

    }

    private static initialize() {
        Authent.authentContainer = document.querySelector('.authent');
        Authent.keyboard = document.querySelector('.authent_keyboard');
        Authent.valid_authent = document.querySelector('.valid_authent');
        Authent.password = document.querySelector('.password');
        Authent.isInitialized = true;

        Authent.authentContainer.classList.add('open');

        Authent.valid_authent.addEventListener('click', (event) => {
            if ( Authent.password.value.length > 5 ) {
                Authent.authentContainer.classList.remove('open');
                setTimeout(() => {
                    document.body.removeChild(Authent.authentContainer);
                }, 300);
            }
        });

        Authent.keyboard.addEventListener('click', (event) => {
            Authent.enterPassword();
        });
    }

    private static enterPassword() {
        if ( Authent.password.value.length < 6 ) {
            Authent.password.value += '1';
        }
    }
}