import stateManager from "./state-manager";
import dispatcher from "./dispatcher";

const header: Element = document.querySelector(".header");

header.addEventListener("click", () =>
{
    stateManager.goBack(1);
});

dispatcher.register("page-1-hide", showHeader1);
dispatcher.register("page-1-show", showHeader2);

function showHeader1()
{
    header.classList.remove("page-1");
}

function showHeader2()
{
    header.classList.add("page-1");
}
