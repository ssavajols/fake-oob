import notificationEntity from './notification-entity';
import {localStorage as isLocalStorageEnabled} from '../configuration/configuration';
import localNotifications from './local-notifications';
import dispatcher from './dispatcher';
import notificationFactory from './notification-factory';

export default class listNotification {
    private static list: notificationEntity[] = [] ;
    private static container: HTMLElement;
    private static localStorageEnabled: boolean = isLocalStorageEnabled;

    public static init(): void
    {
        this.restore();
        this.container = document.getElementById('notification-list');
        this.update();
    }

    public static cleanStorage(): void
    {
        if ( ! this.localStorageEnabled ) {
            return;
        }
        window.localStorage.setItem('notification-list', JSON.stringify([]));
    }

    public static save(): void
    {
        if ( ! this.localStorageEnabled) {
            return;
        }
        window.localStorage.setItem('notification-list', JSON.stringify(this.list));
    }

    public static restore(): void
    {
        if ( ! this.localStorageEnabled ) {
            return;
        }
        try {
            this.list = JSON.parse(localStorage.getItem('notification-list'));
        }catch (e)
        {
            this.list = [];
            console.info(e);
        }

        if ( !this.list )
        {
            this.list = [];
        }
    }

    public static sync(notifications: notificationEntity[]): void
    {
        this.list = this.list.filter((notif: notificationEntity) =>
         {
             let keep = false;

             notifications.forEach((item: notificationEntity) =>
               {
                   if ( notif.notifId === item.notifId )
                   {
                       keep = true;
                   }
               });

             return keep;
         });

        notifications.forEach((notification: notificationEntity) =>
        {
            this.add(notification);
        });

        this.save();
        this.update();
    }

    public static add(notification: notificationEntity): void
    {
        if ( !this.exist(notification) )
        {
            localNotifications.send(notification);
            this.list.push(notification);
            this.save();
            this.update();
        }
    }

    public static clear(): void
    {
        this.list.length = 0;
        this.save();
        this.update();
    }

    public static remove(notification: notificationEntity): void
    {
        for ( let index = 0; index < this.list.length; index++)
        {
            if ( notification.notifId === this.list[index].notifId )
            {
                this.list.splice(index, 1);
                break;
            }
        }

        this.save();
        this.update();
    }

    public static update(): void
    {
        if ( this.container === null )
        {
            return;
            // throw new Error('no notification container available');
        }

        this.container.innerHTML = '';

        if ( !this.list )
        {
            return;
        }

        for ( const notification of this.list )
        {
            const notif = notificationFactory.create(notification);
            const notifNode = notif.toDOMNode();

            notifNode.addEventListener('click', () =>
            {
                dispatcher.dispatch('notification-click', notif);
            });

            this.container.appendChild(notifNode);
        }
    }

    public static exist(notification: notificationEntity): boolean
    {
        let bool = false;

        this.list.forEach((item) =>
        {
            if ( item.notifId === notification.notifId )
            {
                bool = true;
            }
        });

        return bool;
    }
}