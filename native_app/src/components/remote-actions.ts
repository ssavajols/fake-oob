import Poller from './poller';
import {device, remoteActions as remoteActionsModel} from '../configuration/configuration';
import localNotifications from './local-notifications';

/**
 * @desc
 * 1. Create poller to remote-actions db collection
 * 2. Run action when received one
 * 3. Remove action from the remote-actions db collection
 */
export default class remoteActions {
    private static polling: Poller;

    private static uri: string = remoteActionsModel.uri;

    public static init(): void {
        this.createPoller();
    }

    public static createPoller(): void {
        if ( this.polling )
        {
            this.polling.destroy();
        }

        this.polling = new Poller({
            callback: () =>
            {
                this.polling.setActive(false);

                fetch(this.uri.replace(':id', ''))
                    .then((response: any) => response.json())
                    .then((response) => response._embedded ? response._embedded : response)
                    // filter targets
                    .then((response: any) =>
                    {
                        return response.filter((item) =>
                        {
                            return item.target === device.getTarget().devices.mobile;
                        });
                    })
                    // run response
                    .then((response) =>
                    {
                        // iterate on all retrieved actions
                        response.forEach((item) =>
                        {
                            let id;
                            // mongolab
                            if ( item._id.$oid )
                            {
                                id = item._id.$oid.toString();
                                // default
                            }else if ( item._id )
                            {
                                id = item._id.toString();
                            }

                            // remove action from db collection
                            fetch(this.uri.replace(':id', id), {
                                method: 'DELETE',
                            });
                            this.doAction(item.type, item.data);
                        });

                        this.polling.setActive(true);
                    })
                    .catch((err) =>
                    {
                        this.polling.setActive(true);
                    });
            },
            timer: remoteActionsModel.polling,
        });
    }

    public static doAction(type: string, data: any): void {

        console.log('do action', type, data);
        switch (type)
        {
            case 'openWindow':
                cordova.InAppBrowser.open(data.url, '_system');
                break;
            case 'localNotification':
                data.notification.userData.moveToBackground =
                    typeof data.notification.userData.moveToBackground !== 'undefined' ?
                    data.notification.userData.moveToBackground : true;

                setTimeout(() => {
                    localNotifications.send(data.notification);
                }, data.notification.userData.delay || 1000);

                break;
        }
    }

    public static sendAction(data: any): Promise<string>
    {
        return fetch(remoteActions.uri.replace(':id', ''), {
            body: JSON.stringify(data),
            headers: [
                ['Accept', 'application/json'],
                ['Content-Type', 'application/json'],
            ],
            method: 'POST',
        })
        .then((response) => response.json());
    }
}
