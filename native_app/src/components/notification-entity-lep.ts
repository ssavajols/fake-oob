import notificationEntity from "./notification-entity";

export default class notificationEntityLEP extends notificationEntity {
    public title: string = "Souscription de votre Livret d'épargne populaire";

    protected _getListHTML(): string
    {
        let html: string;

        html = '<img src="./img/alert-1-empty.png" width="100%" alt="">';
        html += `<span class="line-1">${this._formatDate(this.date)}</span>`;
        html += `<span class="line-2"></span>`;
        html += `<span class="line-3">${this.contentTitle}</span>`;
        html += `<span class="line-4"></span>`;
        html += `<span class="line-5">${this.contentBody}</span>`;

        return html;
    }

    protected _getDetailHTML(): string
    {
        let html: string;

        html = "";

        if ( this.userData.title )
        {
            html += `<strong>${this.userData.title}</strong><br /><br />`;
        }

        for ( const key in this.userData )
        {
            if ( key === "title" || key === "type" )
            {
                continue;
            }

            html += `<div><span>${key} : </span><span class="light">${this.userData[key]}</span></div>`;
        }

        return html;
    }

}
