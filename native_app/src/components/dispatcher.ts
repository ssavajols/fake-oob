/**
 * @desc
 * 1. Register events on document
 * 2. Dispatch events on document
 */
export default class dispatcher {
    public static dispatch(name: string, data?: object): void {
        const evt = new Event(name);
        evt.data = data;
        document.dispatchEvent(evt);
    }

    public static register(name: string, callback: (event: Event) => void): void {
        document.addEventListener(name, callback);
    }
}