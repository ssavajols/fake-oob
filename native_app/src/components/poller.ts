import Timer = NodeJS.Timer;
/**
 * @desc
 * 1. Create polling
 */
export default class Poller {

    private timerInstance: Timer;

    private isActive: boolean = true;

    private options: PollerOptions = {
        callback: (err, response) => { return; },
        timer: 3000,
    };

    constructor(params?: PollerOptions){
        this.options = {...this.options, ...params};

        if ( isNaN(params.timer) && params.timer < 0 )
        {
            throw new Error("Poller: timer must be a positive number");
        }

        if ( typeof params.callback !== "function" )
        {
            throw new Error("Poller: callback is not a function");
        }

        setTimeout(this.loop.bind(this), 1);
    }

    public setActive(bool: boolean): void {
        this.isActive = bool;
    }

    public getActive(): boolean {
        return this.isActive;
    }

    public loop(): void {

        // clear just in case
        clearTimeout(this.timerInstance);

        // loop all the time
        this.timerInstance = setTimeout(() =>
        {
            this.loop();
        }, this.options.timer);

        // run callback if active
        if ( this.getActive() )
        {
            this.options.callback(null, null);
        }
    }

    public destroy(): void {
        clearTimeout(this.timerInstance);
        this.options = null;
    }
}