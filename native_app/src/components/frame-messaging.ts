import { Natif } from "src/components/natif";

export const frameMessaging = {
    init: function() {
        window.addEventListener('message', (event) => {
            const data = JSON.parse(event.data);
            console.log('received message:', event);
        
            switch ( data.type ) {
                case "syncLocalStorageRequest":
                    Array.prototype.forEach.call(document.querySelectorAll('iframe'), (iframe) => {
                        iframe.contentWindow.postMessage(JSON.stringify({
                            'type': 'syncLocalStorage',
                            'SERVICE_IP': localStorage.getItem('SERVICE_IP'),
                            'SERVICE_IP_VALUE': localStorage.getItem('SERVICE_IP_VALUE'),
                            'device-id': localStorage.getItem('device-id'),
                            'notification-list': localStorage.getItem('notification-list'),
                        }), '*');
                    });
                    break;

                case "cordovaFunction":
                    if ( !data.action ) {
                        return;
                    }
                    if ( Natif[data.action] ) {
                        Natif[data.action].apply(Natif, data.arguments);
                    }
                    break;
            }
        });
    }
};